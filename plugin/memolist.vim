
" DESCRIPTION:  Memolist main plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Check if script is loaded already or if the user does not want it loaded
if exists("g:loaded_memolist")
    finish
endif
let g:loaded_memolist = 1

" Set the options

" variables
if has('win16') || has('win32') || has ('win95') || has('win64')
    let s:path_separator = '\'
else " assume UNIX based
    let s:path_separator = '/'
endif
if !exists('g:memolist_path')
    if has('win16') || has('win32') || has ('win95') || has('win64')
        let g:memolist_path = $HOME.'\.config\notes\' " under your documents and settings 
    else " assume UNIX based
        let g:memolist_path = $HOME."/.config/notes/"
    endif
" Set the marvim repository explicitly (if defined by user)
else
    if g:memolist_path[-1:] != s:path_separator
        " add a trailing directory slash if it does not exist
        let g:memolist_path = g:memolist_path . s:path_separator
    endif
endif

if !exists('g:memolist_prg')
    let g:memolist_prg = ""
endif

" Set the commands

if g:memolist_prg == "fzf"
    " command for listing memos
    command! -bang -nargs=* MemosLst call memolist#memolist#FZFMemolist('Files<bang> '. g:memolist_path)
    if !exists(":RAg")
        " 'raw' ag helper command for grepping
        command! -nargs=+ -complete=file RAg call fzf#vim#ag_raw(<q-args>)
    endif
else
    " command for listing memos
    command! -bang -nargs=* MemosLst call memolist#memolist#VimMemolist()
endif

" Set the mappings

" list notes
nnoremap <silent> <Plug>MemosLst :MemosLst<CR>
" create a new one
nnoremap <silent> <Plug>MemosNew :call memolist#memolist#MemoCreate()<CR>
" delete an existing one
nnoremap <silent> <Plug>MemosDel :call memolist#memolist#MemoDelete()<CR>
" grep for text
nnoremap <silent> <Plug>MemosGrp :call memolist#memolist#MemoGrep()<CR>
" grep for a category
nnoremap <silent> <Plug>MemosGat :call memolist#memolist#MemoGrepCats()<CR>
