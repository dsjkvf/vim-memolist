# Memolist for Vim

## About

This is a Vim plugin for managing a memos collection. A user can enter new notes (based on a configurable template -- see `autoload/memolist/memolist.vim` for that), delete old ones, inspect the list, grep for text or for a category. By default, utilizing only Vim native means (like location lists or `vimgrep`), Memolist can also make use of `fzf` if you have [one](https://github.com/junegunn/fzf) [installed](https://github.com/junegunn/fzf.vim).

## Configuration

To configure, edit your `.vimrc` accordingly:

  - to set a directory for the repository (default is `~/.config/notes`):

        let g:memolist_path = $VIMHOME . '/notes/'

  - to enable `fzf` (default is disabled)

        let g:memolist_prg = 'fzf'

  - to set a mapping to list memos (default is none):

        nmap <Leader>ml <Plug>MemosLst

  - to set a mapping to create a new memo (default is none):

        nmap <Leader>mn <Plug>MemosNew

  - to set a mapping to delete a memo (default is none):

        nmap <Leader>md <Plug>MemosDel

  - to set a mapping to grep memos repository for text (default is none):

        nmap <Leader>mg <Plug>MemosGrp

  - to set a mapping to grep memos for a category (default is none):

        nmap <Leader>mc <Plug>MemosGat

## Screencast

![screencast](http://i.imgur.com/taVmSrE.gif)

Actions shown in the screencast:

  - listing memos
  - grepping memos for 'lorem'
  - grepping memos' categories for 'week'
  - creating a new memo, naming it 'friday'
  - listing memos again
  - deleting the memo named 'friday'
