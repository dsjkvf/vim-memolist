
" DESCRIPTION:  Memolist autoload plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Create a new memo

function! memolist#memolist#MemoCreate()
    let mem_name = input("Name? ")
    let mem_file = g:memolist_path . mem_name . '.' . strftime("%Y-%m-%d.txt")
    if !empty(glob(mem_file))
        " will almost never happen
        execute "edit " . mem_file
    else
        " will happen most often
        let mem_cate = input("Category? ")
        if exists(":Startify")
            tabnew
            normal e
        else
            tabnew
        endif
        let head_1 = '<!-- vim: set ft=mkd nospell foldlevel=200: -->'
        1put =head_1
        let head_2 = '> date: ' . strftime("%a %d %b %Y | %T")
        2put =head_2
        let head_3 = '> categories: [' . mem_cate . ']'
        3put =head_3
        let head_4 = '> title: ' . mem_name
        4put =head_4
        5put ='================================================================================'
        execute 'save ' . fnameescape(mem_file)
        normal ggdd
        normal G
        normal o
    endif
endfunction

" List, grep memos

if g:memolist_prg == "fzf"

    " list entries
    function! memolist#memolist#FZFMemolist(cmd)
        let orig = $FZF_DEFAULT_OPTS
        let orig_prg = $FZF_DEFAULT_COMMAND
        if orig_prg[0:1] == "rg"
            try
                let $FZF_DEFAULT_OPTS .= ' --exact'
                let $FZF_DEFAULT_COMMAND .= ' --maxdepth 1'
                execute a:cmd
            finally
                let $FZF_DEFAULT_OPTS = orig
                let $FZF_DEFAULT_COMMAND = orig_prg
            endtry
        elseif orig_prg[0:1] == "ag"
            try
                let $FZF_DEFAULT_OPTS .= ' --exact'
                let $FZF_DEFAULT_COMMAND .= ' --norecurse'
                execute a:cmd
            finally
                let $FZF_DEFAULT_OPTS = orig
                let $FZF_DEFAULT_COMMAND = orig_prg
            endtry
        else
            try
                let $FZF_DEFAULT_OPTS .= ' --exact'
                let $FZF_DEFAULT_COMMAND = 'find . -mindepth 1 -maxdepth 1 -type f'
                execute a:cmd
            finally
                let $FZF_DEFAULT_OPTS = orig
                let $FZF_DEFAULT_COMMAND = orig_prg
            endtry
        endif
    endfunction

    " contents
    function! memolist#memolist#MemoGrep()
        let mem_pat = input("Pattern? ")
        let mem_com = ":RAg -A2 -B2 " . mem_pat . ' ' . expand(g:memolist_path)
        execute mem_com
    endfunction

    " categories
    function! memolist#memolist#MemoGrepCats()
        let mem_cat = input("Category? ")
        let mem_com = ':RAg "categories: \[.*' . mem_cat . '.*\]" ' . expand(g:memolist_path)
        execute mem_com
    endfunction

else

    " list entries
    function! memolist#memolist#VimMemolist()
        " https://reddit.com/comments/6o6slx//dkf60b8/
        call setloclist(0, map(
            \ systemlist('find ' . g:memolist_path . ' -mindepth 1 -maxdepth 1 -type f'), 
            \ {_, p -> {'filename': p}})
            \ )
        lopen
        setlocal modifiable
        silent %s/.txt//
        silent %s/|| //
        silent %s/\/\//\//
        normal gg
        setlocal nomodifiable
        setlocal nomodified
    endfunction

    " grep contents
    function! memolist#memolist#MemoGrep()
        let mem_pat = input("Pattern? ")
        try
            let mem_com = ":lvimgrep /" . mem_pat . '/j ' . expand(g:memolist_path) . '*'
            execute mem_com
            lopen
        catch
            redraw
            echom "No results found"
        endtry
    endfunction

    " grep categories
    function! memolist#memolist#MemoGrepCats()
        let mem_cat = input("Category? ")
        try
            let mem_com = ':lvimgrep /categories: \[.*' . mem_cat . '.*\]/j ' . expand(g:memolist_path) . '*'
            execute mem_com
            lopen
        catch
            redraw
            echom "No results found"
        endtry
    endfunction

endif

" Delete memos

" helper
function! memolist#memolist#DeleteFile(...)
    if ( &ft == 'help' )
        echohl WarningMsg
        echo 'Cannot delete a help buffer!'
        echohl None
        return -1
    elseif(exists('a:1'))
        let del_file=a:1
    else
        let del_file=expand('%:p')
    endif
    let del_conf = input('Are you sure? [y]/n ')
    if del_conf == '' || del_conf == 'y'
        call delete(del_file)
        if !empty(glob(del_file))
            echo "\r"
            echohl WarningMsg
            echo 'Failed to delete'
            echohl None
        else
            echo "\r"
            echohl WarningMsg
            echo 'Deleted'
            echohl None
        endif
    else
        redraw
        echohl WarningMsg
        echo 'Cancelled'
        echohl None
    endif
endfunction

" main function
function! memolist#memolist#MemoDelete()
    let mem_arg = input('Which memo? ', g:memolist_path, 'file')
    call memolist#memolist#DeleteFile(expand(mem_arg))
endfunction
